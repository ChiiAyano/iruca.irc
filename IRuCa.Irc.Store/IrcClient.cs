﻿// このコードは、ロバート・ローランド氏によるircrtのコードを一部流用しています。
// https://github.com/robertrolandorg/ircrt

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using IRuCa.Irc.Enums;
using IRuCa.Irc.Events;
using IRuCa.Irc.Events.NumericReply;
using OTech.Net.Socket;
using OTech.Net.Socket.General;
using System.Reflection;
using System.Threading.Tasks;

namespace IRuCa.Irc
{
    public class IrcClient : IDisposable
    {
        private TcpSocket ircSocket;

        private readonly char prefixChar = ':';
        private readonly char delimiterChar = ' ';
        private readonly char lineEndChar = '\n';
        private readonly string prefixString = ":";
        private readonly string delimiterString = " ";
        private readonly string lineEndString = "\n";
        private readonly char carriageReturn = '\r';

        private Dictionary<string, Action<IrcMessage>> responseActions;
        private List<WhoisData> whoisData;
        private List<IrcUser> whowasData;

        private List<IrcUser> knownUsers = new List<IrcUser>();
        private List<IrcChannel> joinedChannels = new List<IrcChannel>();


        #region プロパティ

        /// <summary>
        /// 現在接続中か取得します。
        /// </summary>
        public bool IsConnected { get; private set; }

        ///// <summary>
        ///// 現在の自身の nick を取得します。
        ///// </summary>
        //public string CurrentNick { get; private set; }

        /// <summary>
        /// 現在の自身のユーザー情報を取得します。
        /// </summary>
        public IrcUser CurrentUser { get; private set; }

        /// <summary>
        /// 接続するホスト名を取得または設定します。
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// 接続するポート番号を取得または設定します。
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// SSL 接続を許可するか取得または設定します。
        /// </summary>
        public bool IsSsl { get; set; }

        /// <summary>
        /// 通信の文字コードを取得または設定します。
        /// </summary>
        public Encoding Encoding { get; set; }

        #endregion

        #region イベント

        public event EventHandler Connected;
        public event EventHandler Disconnected;
        public event EventHandler<IrcErrorEventArgs> IrcError;
        public event EventHandler<IrcServerMessageEventArgs> ServerMessage;
        public event EventHandler Registered;
        public event EventHandler<IrcPingEventArgs> Ping;
        public event EventHandler<IrcMessageSentEventArgs> MessageSent;
        public event EventHandler<IrcReceiveMessageEventArgs> ReceivePrivmsg;
        public event EventHandler<IrcReceiveMessageEventArgs> ReceiveNotice;
        public event EventHandler<IrcCommandMessageEventArgs> Join;
        public event EventHandler<IrcCommandMessageEventArgs> Quit;
        public event EventHandler<IrcCommandMessageEventArgs> Part;
        public event EventHandler<IrcTopicChangedEventArgs> Topic;
        public event EventHandler<IrcInviteEventArgs> Invite;
        public event EventHandler<IrcKickEventArgs> Kick;

        public event EventHandler<IrcWelcomeEventArgs> ReplyWelcome;
        public event EventHandler<IrcYourHostEventArgs> ReplyYourHost;
        public event EventHandler<IrcCreatedEventArgs> ReplyCreated;
        public event EventHandler<IrcMyInfoEventArgs> ReplyMyInfo;
        public event EventHandler<IrcAwayEventArgs> ReplyAway;
        public event EventHandler<IrcEventArgs> ReplyNoAway;
        public event EventHandler<IrcWhoisEventArgs> ReplyWhois;
        public event EventHandler<IrcWhoWasEventArgs> ReplyWhoWas;
        public event EventHandler<IrcChannelModeEventArgs> ReplyChannelMode;
        public event EventHandler<IrcInviteCompletedEventArgs> ReplyInviteCompleted;

        public event EventHandler<IrcErrorNickInUseEventArgs> ErrorNickInUse;

        #endregion

        #region コンストラクター

        public IrcClient()
            : this(string.Empty, 6667, false)
        {
        }

        public IrcClient(string host, int port)
            : this(host, port, false)
        {
        }

        public IrcClient(string host, int port, bool ssl)
        {
            this.ircSocket = new TcpSocket(host, port, ssl);

            this.Host = host;
            this.Port = port;
            this.IsSsl = ssl;
            this.Encoding = Encoding.UTF8;

            // ソケット上のイベントのリレー
            this.ircSocket.Connected += (s, e) => OnConnected();
            this.ircSocket.Disconnected += (s, e) =>
                {
                    OnDisconnected();
                };
            this.ircSocket.MessageReceived += ircSocket_MessageReceived;

            InitializeResponseAction();
        }

        private void InitializeResponseAction()
        {
            if (this.responseActions == null)
                this.responseActions = new Dictionary<string, Action<IrcMessage>>();

            // レスポンス対応の初期化
            var me = typeof(IrcClient);
            var methods =
                me.GetRuntimeMethods().Select(s => new
                                                       {
                                                           Method = s,
                                                           Attributes = s.GetCustomAttributes(typeof(IrcReplyAttribute))
                                                       }).Where(w => w.Attributes != null);
            foreach (var item in methods)
            {
                foreach (
                    var attribute in
                        item.Attributes.Cast<IrcReplyAttribute>()
                            .Where(attribute => !this.responseActions.ContainsKey(attribute.ReplyType)))
                {
                    this.responseActions.Add(attribute.ReplyType, i => item.Method.Invoke(this, new object[] {i}));
                }
            }
        }

        //private string bufferString = string.Empty;
        private List<byte> bufferBytes = new List<byte>();
        private bool IsMessageReceiving { get; set; }

        private void ircSocket_MessageReceived(object sender, ReceiveMessageEventArgs e)
        {
            this.IsMessageReceiving = true;
            //this.bufferString = this.bufferString.Insert(this.bufferString.Length,
            //                                             this.Encoding.GetString(e.Message, 0, e.Message.Length));
            this.bufferBytes.AddRange(e.Message);

            // 取得したメッセージが改行で終わっていない場合は、改行が来るのを待つ
            //if (!this.bufferString.EndsWith("\r\n") | !this.bufferString.EndsWith("\r") | !this.bufferString.EndsWith("\n"))
            //	return;
            //if (!this.bufferString.EndsWith("\n"))
            //    return;

            // 0x0a = \n
            if (this.bufferBytes.Last() != 0x0a)
                return;

            var message = this.Encoding.GetString(this.bufferBytes.ToArray(), 0, this.bufferBytes.Count);
            this.bufferBytes.Clear();

            // 改行ごとにメッセージを区切る
            var split = message.Split(new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries);

            foreach (var parsed in split.Select(m => MessageParser(m.Trim('\n', '\r'))))
            {
                OnServerMessage(parsed);

                if (this.responseActions.ContainsKey(parsed.CommandName))
                {
                    var action = this.responseActions[parsed.CommandName];
                    action.Invoke(parsed);
                }
            }
            this.IsMessageReceiving = false;
        }

        #endregion

        #region イベント発火

        private void OnConnected()
        {
            this.IsConnected = this.ircSocket.IsConnected;

            if (this.Connected != null)
                this.Connected(this, EventArgs.Empty);
        }

        private void OnDisconnected()
        {
            this.IsConnected = false;

            if (this.Disconnected != null)
                this.Disconnected(this, EventArgs.Empty);
        }

        private void OnServerMessage(IrcMessage ircMessage)
        {
            Debug.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " \"" + ircMessage.RawMessage + "\"");

            if (this.ServerMessage != null)
                this.ServerMessage(this, new IrcServerMessageEventArgs(ircMessage));
        }

        private void OnRegistered()
        {
            if (this.Registered != null)
                this.Registered(this, EventArgs.Empty);
        }

        #endregion

        private IrcMessage MessageParser(string rawMessage)
        {
            return new IrcMessage(rawMessage);
        }

        #region 接続/切断

        public void Connect()
        {
            if (string.IsNullOrWhiteSpace(this.Host))
                throw new IrcConnectionException("Host name is empty.");

            this.ircSocket.Connect(this.Host, this.Port, this.IsSsl);

        }

        public void Disconnect()
        {
            // TODO: 切断処理もここで書いておかねばならない
            this.ircSocket.Dispose();
            this.ircSocket = null;

            OnDisconnected();
        }

        public void Dispose()
        {
            if (this.ircSocket == null) return;

            Disconnect();
        }

        #endregion

        #region コマンド

        private async void SendIrcMessage(IrcMessage message)
        {
            if (!this.ircSocket.IsConnected)
                return;

            var msg = this.Encoding.GetBytes(message.RawMessage + "\r\n");
            this.ircSocket.Send(msg);

            Debug.WriteLine("Sent Message: \"" + message.RawMessage + "\"");

            if (this.MessageSent != null)
                this.MessageSent(this, new IrcMessageSentEventArgs(message));
        }

        private async void SendIrcMessage(string message)
        {
            if (!this.ircSocket.IsConnected)
                return;

            var msg = this.Encoding.GetBytes(message + "\r\n");
            this.ircSocket.Send(msg);

            Debug.WriteLine("Sent Message: \"" + message + "\"");

            if (this.MessageSent != null)
                this.MessageSent(this, new IrcMessageSentEventArgs(new IrcMessage(message)));
        }

        /// <summary>
        /// レジスト メッセージを送信し、IRC サーバーへログインを試みます。
        /// </summary>
        /// <param name="serverPassword"></param>
        /// <param name="nick"></param>
        /// <param name="userName"></param>
        /// <param name="realName"></param>
        public void SendRegister(string serverPassword, string nick, string userName, string realName)
        {
            if (!string.IsNullOrWhiteSpace(serverPassword))
                SendServerPassword(serverPassword);

            SendNick(nick);
            SendUser(userName, realName);
        }

        /// <summary>
        /// IRC サーバーに、新しい nick を通知します。
        /// </summary>
        /// <param name="nick"></param>
        public void SendNick(string nick)
        {
            var message = new IrcMessage(string.Empty, "NICK", nick);
            SendIrcMessage(message);
        }

        /// <summary>
        /// IRC サーバーに、ユーザー情報を通知します。このメッセージは、接続した直後に使用します。
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="realName"></param>
        public void SendUser(string userName, string realName)
        {
            var message = new IrcMessage(string.Empty, "USER", userName, "8", "*", ":" + realName);
            SendIrcMessage(message);
        }

        /// <summary>
        /// IRC サーバーに接続するためのパスワードを送信します。
        /// </summary>
        /// <param name="password"></param>
        public void SendServerPassword(string password)
        {
            if (string.IsNullOrWhiteSpace(password))
                return;

            var message = new IrcMessage(string.Empty, "PASS", password);
            SendIrcMessage(message);
        }

        /// <summary>
        /// PONG メッセージを送信します。
        /// </summary>
        /// <param name="target"></param>
        public void SendPong(string target)
        {
            SendIrcMessage("PONG " + target);
        }

        /// <summary>
        /// 本日のメッセージ (Message Of The Day) を返すようサーバーに要求します。<paramref name="target"/> が空の場合、現在接続中のサーバーに要求します。
        /// </summary>
        /// <param name="target"></param>
        public void SendMotd(string target)
        {
            SendIrcMessage(new IrcMessage(string.Empty, "MOTD", target));
        }

        /// <summary>
        /// IRC ネットワークの統計情報を要求します。
        /// </summary>
        /// <param name="mask"></param>
        /// <param name="target"></param>
        public void SendLusers(string mask, string target)
        {
            SendIrcMessage(new IrcMessage(string.Empty, "LUSERS", mask, target));
        }

        /// <summary>
        /// 指定したサーバーの、IRC サーバー プログラムのバージョン情報を要求します。<paramref name="target"/> が空の場合、接続中のサーバーに要求します。
        /// </summary>
        /// <param name="target"></param>
        public void SendVersion(string target)
        {
            SendIrcMessage(new IrcMessage(string.Empty, "VERSION", target));
        }

        // Linksコマンドはちょっと保留

        /// <summary>
        /// 指定したサーバーのローカル時間を要求します。<paramref name="target"/> が空の場合、接続中のサーバーに要求します。
        /// </summary>
        /// <param name="target"></param>
        public void SendTime(string target)
        {
            SendIrcMessage(new IrcMessage(string.Empty, "TIME", target));
        }

        // Traceコマンドはちょっと保留

        /// <summary>
        /// 指定したサーバーのサーバー情報を要求します。<paramref name="target"/> が空の場合、接続中のサーバーに要求します。
        /// </summary>
        /// <param name="target"></param>
        public void SendInfo(string target)
        {
            SendIrcMessage(new IrcMessage(string.Empty, "INFO", target));
        }

        /// <summary>
        /// 離席状態を設定し、個人宛の PRIVMSG を受信したときの自動応答メッセージを設定します。<paramref name="text"/> が空の場合、離席状態を解除します。
        /// </summary>
        /// <param name="text"></param>
        public void SendSetAway(string text)
        {
            SendIrcMessage(new IrcMessage(string.Empty, "AWAY", text));
        }

        #region 入退室/切断

        /// <summary>
        /// IRC サーバーから切断するコマンドを送信します。送信後、サーバーから切断されます。
        /// </summary>
        /// <param name="message"></param>
        public void SendQuit(string message)
        {
            SendIrcMessage(new IrcMessage(string.Empty, "QUIT", message));
        }

        /// <summary>
        /// 指定されたチャンネルへ入室します。存在しない場合は新しく作成されます。
        /// </summary>
        /// <param name="channel"></param>
        public void SendJoin(string channel)
        {
            SendJoin(channel, string.Empty);
        }

        /// <summary>
        /// 指定したチャンネルへ入室します。存在しない場合は新しく作成されます。
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="password"></param>
        public void SendJoin(string channel, string password)
        {
            SendIrcMessage(new IrcMessage(string.Empty, "JOIN", channel, password));
        }

        /// <summary>
        /// 指定したチャンネルから退室します。
        /// </summary>
        /// <param name="channel"></param>
        public void SendPart(string channel)
        {
            SendPart(channel, string.Empty);
        }

        /// <summary>
        /// 指定したチャンネルから退室します。
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="message"></param>
        public void SendPart(string channel, string message)
        {
            SendIrcMessage(new IrcMessage(string.Empty, "PART", channel, message));
        }

        #endregion

        #region チャンネル設定

        /// <summary>
        /// 指定したチャンネルの状態を設定します。
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="mode"></param>
        public void SendChannelMode(string channel, string mode)
        {
            SendIrcMessage(new IrcMessage(string.Empty, "MODE", channel, mode));
        }

        /// <summary>
        /// 指定したチャンネルのトピックを設定します。<paramref name="topic"/> が空白の場合は、トピックを削除します。
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="topic"></param>
        public void SendChannelTopic(string channel, string topic)
        {
            SendIrcMessage(new IrcMessage(string.Empty, "TOPIC", channel, ":" + topic));
        }

        /// <summary>
        /// 指定したチャンネルの現在のトピックをチェックするコマンドを送出します。
        /// </summary>
        /// <param name="channel"></param>
        public void SendCheckTopic(string channel)
        {
            SendIrcMessage("TOPIC " + channel);
        }

        /// <summary>
        /// 指定したニックネームのユーザーを、指定したチャンネルに招待します。
        /// </summary>
        /// <param name="nick"></param>
        /// <param name="channel"></param>
        public void SendInvite(string nick, string channel)
        {
            SendIrcMessage(new IrcMessage(string.Empty, "INVITE", nick, channel));
        }

        /// <summary>
        /// 指定したチャンネルから、指定したニックネームのユーザーを追い出します。
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="nick"></param>
        public void SendKick(string channel, string nick)
        {
            SendKick(channel, nick, string.Empty);
        }

        /// <summary>
        /// 指定したチャンネルから、指定したニックネームのユーザーを追い出します。
        /// </summary>
        /// <param name="channel"></param>
        /// <param name="nick"></param>
        /// <param name="message"></param>
        public void SendKick(string channel, string nick, string message)
        {
            SendIrcMessage(new IrcMessage(string.Empty, "KICK", channel, nick, message));
        }

        #endregion

        #region ユーザー情報

        // Whoコマンドはちょっと保留

        /// <summary>
        /// サーバーに、指定したユーザーの情報を要求します。
        /// </summary>
        /// <param name="target"></param>
        /// <param name="mask"></param>
        public void SendWhois(string target, params string[] mask)
        {
            var commands = (target + " " + string.Join(",", mask)).Trim();
            SendIrcMessage(new IrcMessage(string.Empty, "WHOIS", commands));
        }

        // Whowasコマンドはちょっと保留
        // たぶん不要かと

        #endregion

        #region メッセージ送信

        /// <summary>
        /// 指定したチャンネルまたはニックネームのユーザーに、メッセージを送信します。
        /// </summary>
        /// <param name="type"></param>
        /// <param name="target"></param>
        /// <param name="message"></param>
        public void SendMessage(SendMessageType type, string target, string message)
        {
            string messageType;
            switch (type)
            {
                case SendMessageType.Privmsg:
                    messageType = "PRIVMSG";
                    break;
                case SendMessageType.Notice:
                    messageType = "NOTICE";
                    break;
                default:
                    messageType = "PRIVMSG";
                    break;
            }

            SendIrcMessage(new IrcMessage(string.Empty, messageType, target, ":" + message));
        }

        /// <summary>
        /// IRC サーバーに、メッセージを直接送信します。
        /// </summary>
        /// <param name="message"></param>
        public void SendRawMessage(string message)
        {
            SendIrcMessage(message);
        }

        #endregion

        #endregion

        #region レスポンス

        private bool IsSameNick(string nick)
        {
            //return this.CurrentNick == nick;
            if (this.CurrentUser == null)
                return false;

            return this.CurrentUser.Nick == nick;
        }

        [IrcReply("PING")]
        private void OnPing(IrcMessage message)
        {
            var ping = General.PingPongRegex.Match(message.RawMessage);
            SendPong(ping.Success ? ping.Groups["host"].Value : string.Empty);

            if (this.Ping != null)
                this.Ping(this, new IrcPingEventArgs(ping.Groups["host"].Value));
        }

        [IrcReply("PRIVMSG")]
        private void OnPrivmsg(IrcMessage message)
        {
            var prefix = General.ParseUserInfoRegex.Match(message.Prefix);
            var nick = string.Empty;
            var ident = string.Empty;
            var host = string.Empty;
            if (prefix.Success)
            {
                nick = prefix.Groups["nick"].Value;
                ident = prefix.Groups["ident"].Value;
                host = prefix.Groups["host"].Value;
            }

            if (this.ReceivePrivmsg != null)
                this.ReceivePrivmsg(this, new IrcReceiveMessageEventArgs(message, nick, ident, host));
        }

        [IrcReply("NOTICE")]
        private void OnNotice(IrcMessage message)
        {
            var prefix = General.ParseMessageRegex.Match(message.RawMessage);
            var nick = string.Empty;
            var ident = string.Empty;
            var host = string.Empty;
            if (prefix.Success)
            {
                nick = prefix.Groups["nick"].Value;
                ident = prefix.Groups["ident"].Value;
                host = prefix.Groups["host"].Value;
            }

            if (this.ReceiveNotice != null)
                this.ReceiveNotice(this, new IrcReceiveMessageEventArgs(message, nick, ident, host));
        }

        [IrcReply("ERROR")]
        private void OnError(IrcMessage message)
        {
            if (this.IrcError != null)
                this.IrcError(this, new IrcErrorEventArgs(message, message.Trailing));
        }

        [IrcReply("JOIN")]
        private void OnJoin(IrcMessage message)
        {
            var prefix = new PrefixPerser(message.Prefix);
            var channel = message.Middle.FirstOrDefault();

            if (this.Join != null)
                this.Join(this,
                          new IrcCommandMessageEventArgs(message, channel, prefix.Nick, prefix.Ident,
                                                         IsSameNick(prefix.Nick), string.Empty));

            if (!IsSameNick(prefix.Nick)) return;

            if (this.joinedChannels.All(a => a.Channel != channel))
            {
                this.joinedChannels.Add(new IrcChannel {Channel = channel});
            }
        }

        [IrcReply("QUIT")]
        private void OnQuit(IrcMessage message)
        {
            var prefix = new PrefixPerser(message.Prefix);
            var channel = message.Middle.FirstOrDefault();

            if (this.Quit != null)
                this.Quit(this,
                          new IrcCommandMessageEventArgs(message, channel, prefix.Nick, prefix.Ident,
                                                         IsSameNick(prefix.Nick),
                                                         message.Trailing));

            if (!IsSameNick(prefix.Nick)) return;

            this.joinedChannels.Clear();
        }

        [IrcReply("PART")]
        private void OnPart(IrcMessage message)
        {
            var prefix = new PrefixPerser(message.Prefix);
            var channel = message.Middle.FirstOrDefault();

            if (this.Part != null)
                this.Part(this,
                          new IrcCommandMessageEventArgs(message, channel, prefix.Nick, prefix.Ident,
                                                         IsSameNick(prefix.Nick),
                                                         message.Trailing));

            if (!IsSameNick(prefix.Nick)) return;

            this.joinedChannels.RemoveAll(r => r.Channel == channel);
        }

        [IrcReply("TOPIC")]
        private void OnTopic(IrcMessage message)
        {
            //var prefix = new PrefixPerser(message.Prefix);
            var user = GetUser(IrcUser.Parse(message.Prefix));
            var channel = message.Middle.FirstOrDefault();

            //if (this.Topic != null)
            //    this.Topic(this,
            //               new IrcCommandMessageEventArgs(message, channel, prefix.Nick, prefix.Ident,
            //                                              IsSameNick(prefix.Nick),
            //                                              message.Trailing));


            var ch = this.joinedChannels.Find(f => f.Channel == channel);
            var index = this.joinedChannels.FindIndex(f => f.Channel == channel);
            ch.Topic = message.Trailing;
            this.joinedChannels[index] = ch;

            if (this.Topic != null)
                this.Topic(this, new IrcTopicChangedEventArgs(message, ch, message.Trailing, user));
        }

        [IrcReply("INVITE")]
        private void OnInvite(IrcMessage message)
        {
            var prefix = new PrefixPerser(message.Prefix);
            var channel = message.Trailing;
            var whom = message.Middle.FirstOrDefault();

            //if (this.Invite != null)
            //    this.Invite(this,
            //                new IrcInviteEventArgs(message, channel, prefix.Nick, prefix.Ident, IsSameNick(prefix.Nick),
            //                                       string.Empty, whom,
            //                                       IsSameNick(whom)));
        }

        [IrcReply("KICK")]
        private void OnKick(IrcMessage message)
        {
            var prefix = new PrefixPerser(message.Prefix);
            var channel = message.Middle.FirstOrDefault();
            var whom = message.Middle.Skip(1).FirstOrDefault();

            if (this.Kick != null)
                this.Kick(this,
                          new IrcKickEventArgs(message, channel, prefix.Nick, prefix.Ident, IsSameNick(prefix.Nick),
                                               message.Trailing, whom,
                                               IsSameNick(whom)));
        }


        #region ニューメリック

        [IrcReply("001")]
        private void OnWelcome(IrcMessage message)
        {
            // Nickの確定
            var welcome = Regex.Match(message.Trailing,
                                      @"Welcome to the Internet Relay Network ((?<nick>[^!]+)!?)?((?<ident>[^@]+)@?)?((?<host>[^ ]+) ?)?");
            if (welcome.Success)
            {
                //this.CurrentNick = welcome.Groups["nick"].Value;
                this.CurrentUser = new IrcUser
                                       {
                                           Nick = welcome.Groups["nick"].Value,
                                           Ident = welcome.Groups["ident"].Value,
                                           Host = welcome.Groups["host"].Value
                                       };
            }

            if (this.ReplyWelcome != null)
                this.ReplyWelcome(this, new IrcWelcomeEventArgs(message, message.Trailing));
        }

        [IrcReply("002")]
        private void OnYourHost(IrcMessage message)
        {
            var match = Regex.Match(message.Trailing,
                                    @"Your host is ((?<servername>[^,]+),?) running version ((?<ver>.*))");
            var serverName = string.Empty;
            var version = string.Empty;

            if (match.Success)
            {
                serverName = match.Groups["servername"].Value;
                version = match.Groups["ver"].Value;
            }

            if (this.ReplyYourHost != null)
                this.ReplyYourHost(this, new IrcYourHostEventArgs(message, serverName, version));
        }

        [IrcReply("003")]
        private void OnCreated(IrcMessage message)
        {
            var match = Regex.Match(message.Trailing, @"This server was created ((?<date>.*))");
            var date = string.Empty;

            if (match.Success)
            {
                date = match.Groups["date"].Value;
            }

            if (this.ReplyCreated != null)
                this.ReplyCreated(this, new IrcCreatedEventArgs(message, date));
        }

        [IrcReply("004")]
        private void OnMyInfo(IrcMessage message)
        {
            var match = Regex.Match(message.Trailing,
                                    @"((?<servername>[^ ]+) ?) ((?<version>[^ ]+) ?) ((?<availableUserModes>[^ ]+) ?) ((?<availableChannelModes>.*))");
            var serverName = string.Empty;
            var version = string.Empty;
            var availableUserModes = string.Empty;
            var availableChannelModes = string.Empty;

            if (match.Success)
            {
                serverName = match.Groups["servername"].Value;
                version = match.Groups["version"].Value;
                availableUserModes = match.Groups["availableUserModes"].Value;
                availableChannelModes = match.Groups["availableChannelModes"].Value;
            }

            if (this.ReplyMyInfo != null)
                this.ReplyMyInfo(this,
                                 new IrcMyInfoEventArgs(message, serverName, version, availableUserModes,
                                                        availableChannelModes));
        }

        // 302 RPL_USERHOST

        // 303 RPL_ISON

        [IrcReply("301")]
        private void OnAway(IrcMessage message)
        {
            var match = Regex.Match(message.RawMessage, @"((?<nick>[^ ]+) ?):((?<message>.*))");
            var nick = string.Empty;
            var awayMessage = string.Empty;

            if (match.Success)
            {
                nick = match.Groups["nick"].Value;
                awayMessage = match.Groups["message"].Value;
            }

            if (this.ReplyAway != null)
                this.ReplyAway(this, new IrcAwayEventArgs(message, nick, awayMessage, IsSameNick(nick)));
        }

        [IrcReply("305")]
        [IrcReply("306")]
        private void OnNoAway(IrcMessage message)
        {
            if (this.ReplyNoAway != null)
                this.ReplyNoAway(this, new IrcEventArgs(message));
        }

        [IrcReply("311")]
        private void OnWhoisUser(IrcMessage message)
        {
            var match = Regex.Match(message.RawMessage,
                                    @"((?<nick>[^ ]+) ?)((?<user>[^ ]+) ?)((?<host>[^ ]+) ?)\* :((?<realname>.*))");

            if (!match.Success) return;

            var nick = match.Groups["nick"].Value;
            var ident = match.Groups["user"].Value;
            var host = match.Groups["host"].Value;
            var realName = match.Groups["realname"].Value;

            var whois = GetWhoisData(nick);
            whois.User = new IrcUser {Nick = nick, Ident = ident, Host = host, RealName = realName};
            //whois.Nick = nick;
            //whois.Ident = ident;
            //whois.Host = host;
            //whois.RealName = realName;
        }

        [IrcReply("312")]
        private void OnWhoisServer(IrcMessage message)
        {
            var match = Regex.Match(message.RawMessage, @"((?<nick>[^ ]+) ?)((?<server>[^ ]+) ?):((?<serverinfo>.*))");

            if (!match.Success) return;

            var nick = match.Groups["nick"].Value;
            var server = match.Groups["server"].Value;
            var serverInfo = match.Groups["serverinfo"].Value;

            var whois = GetWhoisData(nick);
            whois.Server = server;
            whois.ServerInfo = serverInfo;

            if (whois.User == null)
                whois.User = new IrcUser();

            whois.User.ConnectServer = server;
            whois.User.ConnectServerInfo = serverInfo;
        }

        [IrcReply("313")]
        private void OnWhoisOperator(IrcMessage message)
        {
            var match = Regex.Match(message.RawMessage, @"((?<nick>[^ ]+) ?):");

            if (!match.Success) return;

            var nick = match.Groups["nick"].Value;

            var whois = GetWhoisData(nick);
            whois.IrcOperator = true;
        }

        [IrcReply("317")]
        private void OnWhoisIdle(IrcMessage message)
        {
            var match = Regex.Match(message.RawMessage, @"((?<nick>[^ ]+) ?)((?<idle>[^ ]+) ?)((?<signon>[^ ]+) ?):");

            if (!match.Success) return;

            var nick = match.Groups["nick"].Value;
            var time = 0;
            int.TryParse(match.Groups["idle"].Value, out time);

            var whois = GetWhoisData(nick);
            whois.IdleTime = time;
        }

        [IrcReply("318")]
        private void OnEndOfWhois(IrcMessage message)
        {
            var match = Regex.Match(message.RawMessage, @"((?<nick>[^ ]+) ?):");

            if (!match.Success) return;

            var nick = match.Groups["nick"].Value;

            var whois = GetWhoisData(nick);

            if (this.ReplyWhois != null)
                this.ReplyWhois(this, new IrcWhoisEventArgs(message, whois));

            var known =
                this.knownUsers.Select((s, i) => new {Index = i, Value = s})
                    .FirstOrDefault(f => f.Value.Ident == whois.Ident);
            if (known == null)
                this.knownUsers.Add(whois.User);
            else
                this.knownUsers[known.Index] = whois.User;

            this.whoisData.Remove(whois);
        }

        [IrcReply("319")]
        private void OnWhoisChannels(IrcMessage message)
        {
            var match = Regex.Match(message.RawMessage, @"((?<nick>[^ ]+) ?):(?<channel>([^ ]+) ?)*");

            if (!match.Success) return;

            var nick = match.Groups["nick"].Value;
            var channel =
                match.Groups["channel"].Captures.OfType<Capture>().Select(s => new IrcUserJoinedChannel(s.Value)).ToArray();

            var whois = GetWhoisData(nick);
            whois.Channels = channel;

            if (whois.User == null)
                whois.User = new IrcUser();

            whois.User.JoinedChannel = channel;
        }

        /// <summary>
        /// WHOIS データを格納するクラスのインスタンスを返します。指定された <paramref name="nick"/> が見つからない場合は、新しいインスタンスを生成して返します。
        /// </summary>
        /// <param name="nick"></param>
        /// <returns></returns>
        private WhoisData GetWhoisData(string nick)
        {
            if (this.whoisData == null)
                this.whoisData = new List<WhoisData>();

            var whois = this.whoisData.FirstOrDefault(f => f.Nick == nick);

            if (whois == null)
            {
                var w = new WhoisData();
                this.whoisData.Add(w);

                return w;
            }

            return whois;
        }

        [IrcReply("314")]
        private void WhoWasUser(IrcMessage message)
        {
            var match = Regex.Match(message.RawMessage,
                                    @"((?<nick>[^ ]+) ?)((?<user>[^ ]+) ?)((?<host>[^ ]+) ?)\* :((?<realname>.*))");

            if (!match.Success) return;

            if (this.whowasData == null)
                this.whowasData = new List<IrcUser>();

            var nick = match.Groups["nick"].Value;
            var ident = match.Groups["user"].Value;
            var host = match.Groups["host"].Value;
            var realName = match.Groups["realname"].Value;

            var whowas = new IrcUser {Nick = nick, Ident = ident, Host = host, RealName = realName};
            this.whowasData.Add(whowas);
        }

        [IrcReply("369")]
        private void EndWhoWas(IrcMessage message)
        {
            if (this.ReplyWhoWas != null)
                this.ReplyWhoWas(this, new IrcWhoWasEventArgs(message, this.whowasData));
        }

        [IrcReply("324")]
        private void ChannelMode(IrcMessage message)
        {
            var middle = message.Middle.Skip(1).ToArray();

            var channel = string.Empty;
            var mode = string.Empty;
            var modeParam = string.Empty;

            if (middle.Any())
            {
                channel = middle.FirstOrDefault();
                mode = middle.Skip(1).FirstOrDefault();
                modeParam = middle.Skip(2).FirstOrDefault();
            }

            if (this.ReplyChannelMode != null)
                this.ReplyChannelMode(this, new IrcChannelModeEventArgs(message, channel, mode, modeParam));
        }

        [IrcReply("331")]
        private void OnNoTopic(IrcMessage message)
        {
            var middle = message.Middle.Skip(1).ToArray();

            var channel = string.Empty;

            if (middle.Any())
            {
                channel = middle.FirstOrDefault();
            }

            var ircChannel = GetChannel(channel);
            ircChannel.Topic = string.Empty;
        }

        [IrcReply("332")]
        private void OnReplyTopic(IrcMessage message)
        {
            var middle = message.Middle.Skip(1).ToArray();
            var channel = string.Empty;

            if (middle.Any())
            {
                channel = middle.FirstOrDefault();
            }

            var ircChannel = GetChannel(channel);
            ircChannel.Topic = message.Trailing;
        }

        [IrcReply("333")]
        private void OnTopicChange(IrcMessage message)
        {
            var middle = message.Middle.Skip(1).ToArray();
            var channel = string.Empty;
            IrcUser user = null;
            var time = string.Empty;

            if (middle.Any())
            {
                channel = middle.FirstOrDefault();
                IrcUser.TryParse(middle.Skip(1).FirstOrDefault(), out user);
                user = GetUser(user);
                time = middle.Skip(2).FirstOrDefault();
            }

            var ircChannel = GetChannel(channel);
            if (this.Topic != null)
                this.Topic(this, new IrcTopicChangedEventArgs(message, ircChannel, ircChannel.Topic, user));

        }

        [IrcReply("341")]
        private void OnInviting(IrcMessage message)
        {
            // Inviteを投げたことを通知するリプライ。
            // Inviteされた時はINVITEコマンドで返る
            var middle = message.Middle.Skip(1).ToArray();
            var channel = string.Empty;
            var nick = string.Empty;

            if (middle.Any())
            {
                channel = middle.FirstOrDefault();
                nick = middle.Skip(1).FirstOrDefault();
            }

            var user = this.knownUsers.FirstOrDefault(f => f.Nick == nick);
            var ircChannel = GetChannel(channel);

            if (this.ReplyInviteCompleted != null)
                this.ReplyInviteCompleted(this, new IrcInviteCompletedEventArgs(message, ircChannel, user));
        }

        private Regex whoReplyMatch =
            new Regex(
                @"((?<name>[^ ]+) ?)((?<channel>[^ ]+) ?)((?<user>[^ ]+) ?)((?<host>[^ ]+) ?)((?<server>[^ ]+) ?)((?<nick>[^ ]+) ?)((H|G)[\*]?(\@|\+)) :((?<hopcounti>[^ ]+) ?)((?<hopcount>[^ ]+) ?)((?<realname>.*))");

        [IrcReply("352")]
        private async void OnWhoReply(IrcMessage message)
        {
            var reply = await Task.Factory.StartNew(() =>
                {
                    var middle = message.Middle;
                    var user = new IrcUser
                                   {
                                       Ident = middle.Skip(2).First(),
                                       Host = middle.Skip(3).First(),
                                       ConnectServer = middle.Skip(4).First(),
                                       Nick = middle.Skip(5).First(),
                                       RealName = string.Join(" ", message.Trailing.Split(' ').Skip(2))
                                   };
                    return user;
                });

            //if (reply.Success)
            //{
            //    var user = new IrcUser();

            //    user.Nick = reply.Groups["nick"].Value;
            //    user.RealName = reply.Groups["realname"].Value;
            //    user.Ident = reply.Groups["user"].Value;
            //    user.Host = reply.Groups["host"].Value;
            //    user.ConnectServer = reply.Groups["server"].Value;

            var known = this.knownUsers.Find(f => f.Nick == message.Middle.Skip(5).First());
            if (known == null)
            {
                var middle = message.Middle;
                //":(irc.media.kyoto-u.ac.jp 352 )oka_test #伊勢的新常識@ircnet ~btm www12174u.sakura.ne.jp irc.media.kyoto-u.ac.jp btm_home H@ (:0 392Z Taro Matsuzawa)"
                var user = new IrcUser
                               {
                                   Ident = middle.Skip(2).First(),
                                   Host = middle.Skip(3).First(),
                                   ConnectServer = middle.Skip(4).First(),
                                   Nick = middle.Skip(5).First(),
                                   RealName = string.Join(" ", message.Trailing.Split(' ').Skip(2)),
                                   JoinedChannel = new[] {new IrcUserJoinedChannel(middle.Skip(1).First())}
                               };
                this.knownUsers.Add(user);
            }
            else
                known = reply;
        }

        [IrcReply("315")]
        private void OnEndOfWho(IrcMessage message)
        {

        }

        [IrcReply("353")]
        private void OnNameReply(IrcMessage message)
        {
            var match = Regex.Match(message.RawMessage,
                                    @"(@|=|\*) ((?<channel>[^ ].*)) :(?<nick>([^ ]+) *)*");

            if (match.Success)
            {
                var nicks =
                    match.Groups["nick"].Captures.Cast<Capture>()
                                        .Select(s => Regex.Replace(s.Value, @"(@|\+)", string.Empty)).ToArray();
                var channel = this.joinedChannels.FirstOrDefault(f => f.Channel == match.Groups["channel"].Value);
                if (channel == null)
                {
                    channel = new IrcChannel();
                    channel.Users = nicks.Select(s => new IrcUser { Nick = s });
                    this.joinedChannels.Add(channel);
                }

                channel.Users = nicks.Select(s => new IrcUser {Nick = s});
            }
        }

        [IrcReply("366")]
        private void OnEndOfNames(IrcMessage message)
        {
            var channel = message.Middle.LastOrDefault();
            SendIrcMessage("WHO " + channel);
        }

        [IrcReply("433"), IrcReply("436")]
        private void OnErrorNickInUse(IrcMessage message)
        {
            if (this.ErrorNickInUse != null)
                this.ErrorNickInUse(this, new IrcErrorNickInUseEventArgs(message));
        }

        #endregion

        #endregion

        #region 内部処理

        private IrcChannel GetChannel(string channel)
        {
            return this.joinedChannels.Find(f => f.Channel == channel);
        }

        private IrcChannel GetChannel(string channel, out int index)
        {
            var ch = GetChannel(channel);
            index = this.joinedChannels.FindIndex(f => f.Channel == channel);

            return ch;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private IrcUser GetUser(IrcUser user)
        {
            var find = this.knownUsers.Find(f => f.Ident == user.Ident);

            return find ?? user;
        }

        

        #endregion
    }
}
