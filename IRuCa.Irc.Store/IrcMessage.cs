﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using IRuCa.Irc.Enums;

namespace IRuCa.Irc
{
	public class IrcMessage
	{
		public string RawMessage { get; private set; }
		public string CommandName { get; private set; }
		public string Prefix { get; private set; }
		public string[] Middle { get; private set; }
		public string Coda { get; private set; }
		public string Trailing { get; private set; }

		//public MessageType MessageType { get; private set; }
		//public ReplyCode ReplyCode { get; private set; }

		//public string Nick { get; private set; }
		//public string Ident { get; private set; }
		//public string Host { get; private set; }
		//public string Message { get; private set; }

		public IrcMessage(string rawMessage)
		{
			ParseMessage(rawMessage);
		}

		public IrcMessage(string prefix, string command, params string[] parameters)
		{
			//this.Prefix = prefix;
			//this.CommandName = command;
			//this.Middle = parameters;

			// メッセージ形式にしてRawMessage化
			var raw = string.Empty;
			if (!string.IsNullOrWhiteSpace(prefix))
			{
				raw = raw.Insert(raw.Length, ":" + prefix + " ");
			}

			raw = raw.Insert(raw.Length, command + " ");

			if (parameters != null)
			{
				raw = raw.Insert(raw.Length, string.Join(" ", parameters).TrimEnd(' '));
			}

			raw = raw.Insert(raw.Length, "\r\n");

			ParseMessage(raw);
		}

		private void ParseMessage(string rawMessage)
		{
			this.RawMessage = rawMessage;

			// パース
			var match = Regex.Match(rawMessage, General.ParseMessagePattern);

			if (!match.Success) return;

			this.CommandName = match.Groups["Command"].Value;
			this.Prefix = match.Groups["Prefix"].Value;
			this.Middle = match.Groups["middle"].Captures.OfType<Capture>().Select(s => s.Value.Trim()).ToArray(); ;
			this.Coda = match.Groups["coda"].Value;
			this.Trailing = match.Groups["trailing"].Value;

			// ユーザー情報の抜き取り
			//var user = Regex.Match(match.Groups["Prefix"].Value, General.ParseUserInfoRegex);
			//if (user.Success)
			//{
			//	this.Nick = user.Groups["nick"].Value;
			//	this.Ident = user.Groups["ident"].Success ? user.Groups["ident"].Value : string.Empty;
			//	this.Host = user.Groups["host"].Success ? user.Groups["host"].Value : string.Empty;
			//}

			//var code = 0;
			//if (int.TryParse(match.Groups["Command"].Value, out code))
			//	this.ReplyCode = (ReplyCode)code;
			//else
			//	this.ReplyCode = ReplyCode.None;

			//this.Message = match.Groups["coda"].Value.Trim(':');

			//this.MessageType = GetMessageType(rawMessage);
		}

		private MessageType GetMessageType(string rawMessage)
		{
			var match = General.ParseMessageRegex.Match(rawMessage);
			if (!match.Success)
				return Enums.MessageType.Unknown;
				int code;
				var replyCode = ReplyCode.None;
				if (int.TryParse(this.CommandName, out code))
					replyCode = (ReplyCode)code;

			switch (replyCode)
			{
				case ReplyCode.Welcome:
				case ReplyCode.YourHost:
				case ReplyCode.Created:
				case ReplyCode.MyInfo:
				case ReplyCode.Bounce:
					return MessageType.Login;
				case ReplyCode.LUserClient:
				case ReplyCode.LUserOp:
				case ReplyCode.LUserUnknown:
				case ReplyCode.LUserMe:
				case ReplyCode.LUserChannels:
					return MessageType.Info;
				case ReplyCode.MotdStart:
				case ReplyCode.Motd:
				case ReplyCode.EndOfMotd:
					return MessageType.Motd;
				case ReplyCode.NameReply:
				case ReplyCode.EndOfNames:
					return MessageType.Name;
				case ReplyCode.WhoReply:
				case ReplyCode.EndOfWho:
					return MessageType.Who;
				case ReplyCode.List:
				case ReplyCode.ListEnd:
					return MessageType.List;
				case ReplyCode.BanList:
				case ReplyCode.EndOfBanList:
					return MessageType.BanList;
				case ReplyCode.Topic:
				case ReplyCode.NoTopic:
					return MessageType.Topic;
				case ReplyCode.WhoisUser:
				case ReplyCode.WhoisServer:
				case ReplyCode.WhoisOperator:
				case ReplyCode.WhoisIdle:
				case ReplyCode.WhoisChannels:
				case ReplyCode.EndOfWhois:
					return MessageType.WhoIs;
				case ReplyCode.WhoWasUser:
				case ReplyCode.EndWhoWas:
					return MessageType.WhoWas;
				case ReplyCode.ChannelModeIs:
					return MessageType.ChannelMode;
				default:
					if (((int)replyCode >= 400) && ((int)replyCode <= 599))
					{
						return MessageType.ErrorMessage;
					}
					break;
					//else
					//{
					//	Debug.WriteLine("Unknown ReplyCode: {0} ({1})", code, rawMessage);
					//	return Enums.MessageType.Unknown;
					//}
			}
			// 特殊メッセージ
			if (General.PingPongRegex.Match(rawMessage).Success)
				return MessageType.Unknown;
			if (General.ErrorRegex.Match(rawMessage).Success)
				return MessageType.Error;

			// 通常メッセージ
			var pattern = General.ParseMessageRegex.Match(rawMessage);
			if (!pattern.Success)
				return MessageType.Unknown;
			var command = pattern.Groups["Command"].Value;
			var target = this.Middle.First();

			if (command == "PRIVMSG")
			{
				switch (target.ToCharArray().First())
				{
					case '#':
					case '!':
					case '&':
					case '+':
						return MessageType.ChannelMessage;
					default:
						return MessageType.QueryMessage;
				}
			}
			
			if (command == "NOTICE")
			{
				switch (target.ToCharArray().First())
				{
					case '#':
					case '!':
					case '&':
					case '+':
						return MessageType.ChannelNotice;
					default:
						return MessageType.QueryNotice;
				}
			}

			if (command == "INVITE")
			{
				return MessageType.Invite;
			}

			if (command == "JOIN")
			{
				return MessageType.Join;
			}

			if (command == "TOPIC")
			{
				return MessageType.TopicChange;
			}

			if (command == "NICK")
			{
				return MessageType.NickChange;
			}

			if (command == "KICK")
			{
				return MessageType.Kick;
			}

			if (command == "PART")
			{
				return MessageType.Part;
			}

			if (command == "MODE")
			{
				
			}

			if (command == "QUIT")
			{
				return MessageType.Quit;
			}

			// コマンドが解析出来なかった
			Debug.WriteLine("Unknown MessageType: \"{0}\"", rawMessage);
			return MessageType.Unknown;
		}

		public override string ToString()
		{
			return this.RawMessage;
		}
	}
}
