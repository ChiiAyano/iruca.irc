﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc
{
	public class PrefixPerser
	{
		public string Nick { get; private set; }
		public string Ident { get; private set; }
		public string Host { get; private set; }

		public PrefixPerser(string prefix)
		{
			var p = General.ParseUserInfoRegex.Match(prefix);
			if (!p.Success) return;

			this.Nick = p.Groups["nick"].Value;
			this.Ident = p.Groups["ident"].Value;
			this.Host = p.Groups["host"].Value;
		}
	}
}
