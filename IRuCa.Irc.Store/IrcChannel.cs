﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc
{
	public class IrcChannel
	{
		public string Channel { get; internal set; }

		public string Topic { get; internal set; }

		public string Mode { get; internal set; }

		public IEnumerable<string> ModeParams { get; internal set; }

		public IEnumerable<IrcUser> Users { get; internal set; }

		public IEnumerable<IrcUser> OperatorUsers { get; internal set; }

		public bool CheckOperator(IrcUser user)
		{
			return CheckOperator(user.Nick);
		}

		public bool CheckOperator(string nick)
		{
			return this.OperatorUsers.Any(w => w.Nick == nick);
		}
	}
}
