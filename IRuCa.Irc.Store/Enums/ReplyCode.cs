﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Enums
{
	/// <summary>
	/// 応答メッセージの種類を定義します。
	/// </summary>
	public enum ReplyCode
	{
		None = 0,

		Welcome = 1,
		YourHost = 2,
		Created = 3,
		MyInfo = 4,
		Bounce = 5,



		Away = 301,
		UserHost = 302,
		Ison = 303,
		UnAway = 305,
		NowAway = 306,
		WhoisUser = 311,
		WhoisServer = 312,
		WhoisOperator = 313,
		WhoisIdle = 317,
		EndOfWhois = 318,
		WhoisChannels = 319,
		WhoWasUser = 314,
		EndWhoWas = 369,
		List = 322,
		ListEnd = 323,
		UniqOpis = 325,
		ChannelModeIs = 324,
		NoTopic = 331,
		Topic = 332,
		Inviting = 341,
		Summoning = 342,
		InviteList = 346,
		EndOfInviteList = 347,
		ExceptList = 348,
		EndOfExceptList = 349,
		Version = 351,
		WhoReply = 352,
		EndOfWho = 315,
		NameReply = 353,
		EndOfNames = 366,
		Links = 364,
		EndOfLinks = 365,
		BanList = 367,
		EndOfBanList = 368,
		Info = 371,
		EndOfInfo = 374,
		MotdStart = 375,
		Motd = 372,
		EndOfMotd = 376,
		YoureOper = 381,
		ReHashing = 382,
		YourService = 383,
		Time = 391,
		UserStart = 392,
		Users = 393,
		EndOfUsers = 394,
		NoUsers = 395,

		TraceLink = 200,
		TraceConnecting = 201,
		TraceHandShake = 202,
		TraceUnknown = 203,
		TraceOperator = 204,
		TraceUser = 205,
		TraceServer = 206,
		TraceService = 207,
		TraceNewType = 208,
		TraceClass = 209,
		TraceConnect = 210,
		TraceLog = 261,
		TraceEnd = 262,
		StatsLinkInfo = 211,
		StatsCommands = 212,
		EndOfStats = 219,
		StatsUpTime = 242,
		StatsOLine = 243,
		UModeIs = 221,
		ServerList = 234,
		ServerListEnd = 235,
		LUserClient = 251,
		LUserOp = 252,
		LUserUnknown = 253,
		LUserChannels = 254,
		LUserMe = 255,
		AdminMe = 256,
		AdminLoc1 = 257,
		AdminLoc2 = 258,
		AdminEMail = 259,
		TryAgain = 263,

		ErrorNoSuchNick = 401,
		ErrorNoSuchServer = 402,
		NoSuchChannel = 403,
		ErrorCannotSendToChannel = 404,
		ErrorTooManyChannels = 405,
		ErrorWasNoSuchNick = 406,
		ErrorTooManyTargets = 407,
		ErrorNoSuchService = 408,
		ErrorNoOrigin = 409,
		ErrorNoRecipient = 411,
		ErrorNoTextToSend = 412,
		ErrorNoTopLevel = 413,
		ErrorWildTopLevel = 414,
		ErrorBadMask = 415,
		ErrorUnknownCommand = 421,
		ErrorNoMotd = 422,
		ErrorNoAdminInfo = 423,
		ErrorFileError = 424,
		ErrorNoNickNameGiven = 431,
		ErrorErroneousNickName = 432,
		ErrorNickNameInUse = 433,
		ErrorNickCollision = 436,
		ErrorUnavailableSource = 437,
		ErrorUserNotInChannel = 441,
		ErrorNotOnChannel = 442,
		ErrorUserOnChannel = 443,
		ErrorNoLogin = 444,
		ErrorSummonDisabled = 445,
		ErrorUserDisabled = 446,
		ErrorNotRegistered = 451,
		ErrorNeedMoreParameters = 461,
		ErrorAlreadyRegistered = 462,
		ErrorNoPermitForHost = 463,
		ErrorPasswordMismatch = 464,
		ErrorYoureBannedCreep = 465,
		ErrorYouWillBeBanned = 466,
		ErrorKeySe = 467,
		ErrorChannelIsFull = 471,
		ErrorUnknownMode = 472,
		ErrorInviteOnlyChannel = 473,
		ErrorBannedFromChannel = 474,
		ErrorBadChannelKey = 475,
		ErrorBadChannelMask = 476,
		ErrorNoChannelModes = 477,
		ErrorBanListFull = 478,
		ErrorNoPrivileges = 481,
		ErrorChannelOperatorPrivilegesNeeded = 482,
		ErrorCannotKillServer = 483,
		ErrorRestricted = 484,
		ErrorUniqueOperatorPrivilegesNeeded = 485,
		ErrorNoOperHost = 491,
		ErrorUserModeUnknownFlag = 501,
		ErrorUsersDoNotMatch = 502
	}
}
