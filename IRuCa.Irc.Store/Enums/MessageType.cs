﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Enums
{
	/// <summary>
	/// IRC メッセージの種類を定義します。
	/// </summary>
	public enum MessageType
	{
		Info,
		Login,
		Motd,
		List,
		Join,
		Kick,
		Part,
		Invite,
		Quit,
		Who,
		WhoIs,
		WhoWas,
		Name,
		Topic,
		BanList,
		NickChange,
		TopicChange,
		UserMode,
		UserModeChange,
		ChannelMode,
		ChannelModeChange,
		ChannelMessage,
		ChannelAction,
		ChannelNotice,
		QueryMessage,
		QueryAction,
		QueryNotice,
		CtcpReply,
		CtcpRequest,
		Error,
		ErrorMessage,
		Unknown
	}
}
