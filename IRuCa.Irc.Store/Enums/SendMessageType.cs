﻿namespace IRuCa.Irc.Enums
{
	/// <summary>
	/// IRC に送信するメッセージの種類を定義します。
	/// </summary>
	public enum SendMessageType
	{
		/// <summary>
		/// 通常メッセージ (PRIVMSG) を送信します。
		/// </summary>
		Privmsg,
		/// <summary>
		/// 通知メッセージ (NOTICE) を送信します。
		/// </summary>
		Notice
	}
}
