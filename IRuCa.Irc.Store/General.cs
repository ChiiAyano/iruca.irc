﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IRuCa.Irc
{
	public static class General
	{
		public static string ParseMessagePattern
		{
			get { return @"(?::(?<Prefix>[^ ]+) +)?(?<Command>[^ :]+)(?<middle>(?: +[^ :]+))*(?<coda> +:(?<trailing>.*)?)?"; }
		}

		public static Regex ParseMessageRegex
		{
			get { return new Regex(General.ParseMessagePattern); }
		}

		public static string ParseUserInfoPattern
		{
			get { return @":?((?<nick>[^!]+)!?)?((?<ident>[^@]+)@?)?((?<host>[^ ]+) ?)?"; }
		}

		public static Regex ParseUserInfoRegex
		{
			get { return new Regex(General.ParseUserInfoPattern); }
		}

		public static Regex PingPongRegex
		{
			get { return new Regex(@"^P[OI]NG :(?<host>.*)"); }
		}

		public static Regex ErrorRegex
		{
			get { return new Regex(@"^ERROR :(?<message>.*)"); }
		}
	}
}
