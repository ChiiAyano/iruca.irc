﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc
{
	public class IrcUser
	{
		public string Nick { get; internal set; }
		public string Ident { get; internal set; }
		public string Host { get; internal set; }
		public string RealName { get; internal set; }
		public string ConnectServer { get; internal set; }
		public string ConnectServerInfo { get; internal set; }
		public IEnumerable<IrcUserJoinedChannel> JoinedChannel { get; internal set; }

	    public static IrcUser Parse(string prefix)
	    {
	        IrcUser user;

	        return TryParse(prefix, out user) ? user : null;
	    }

	    public static bool TryParse(string prefix,out IrcUser user)
	    {
	        var regex = General.ParseUserInfoRegex.Match(prefix);
	        if (regex.Success)
	        {
	            user = new IrcUser
	                       {
	                           Nick = regex.Groups["nick"].Value,
	                           Ident = regex.Groups["ident"].Value,
	                           Host = regex.Groups["host"].Value
	                       };
                return true;
	        }

	        user = null;
	        return false;
	    }
	}

	public class IrcUserJoinedChannel
	{
		public string Channel { get; private set; }
		public IrcChannelPermission Permission { get; private set; }

		public IrcUserJoinedChannel(string channel)
		{
			var prefix = channel.Substring(0, 1);
			if (prefix == "@")
			{
				this.Permission = IrcChannelPermission.Operator;
				this.Channel = channel.Substring(1);
			}
			if (prefix == "+")
			{
				this.Permission = IrcChannelPermission.Moderator;
				this.Channel = channel.Substring(1);
			}
			if (prefix == "#")
			{
				this.Permission = IrcChannelPermission.None;
				this.Channel = channel;
			}
		}

		public override string ToString()
		{
			var prefix = string.Empty;
			if (this.Permission == IrcChannelPermission.Operator)
				prefix = "[@]";
			if (this.Permission == IrcChannelPermission.Moderator)
				prefix = "[+]";
			return prefix + this.Channel;
		}
	}

	public enum IrcChannelPermission
	{
		None = 0,
		Operator = 1,
		Moderator = 2
	}
}
