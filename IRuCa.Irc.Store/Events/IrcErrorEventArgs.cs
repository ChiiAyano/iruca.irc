﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Events
{
	public class IrcErrorEventArgs : IrcEventArgs
	{
		public string ErrorMessage { get; private set; }

		public IrcErrorEventArgs(IrcMessage message, string errorMessage)
			:base(message)
		{
			this.ErrorMessage = errorMessage;
		}
	}
}
