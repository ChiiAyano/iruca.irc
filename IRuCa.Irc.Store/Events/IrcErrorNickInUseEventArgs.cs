﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Events
{
	public class IrcErrorNickInUseEventArgs : IrcEventArgs
	{
		public IrcErrorNickInUseEventArgs(IrcMessage message)
			: base(message)
		{

		}
	}
}
