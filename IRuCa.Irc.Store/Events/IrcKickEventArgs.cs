﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Events
{
	public class IrcKickEventArgs : IrcEventArgs
	{
		/// <summary>
		/// チャンネルを取得します。
		/// </summary>
		public string Channel { get; private set; }
		/// <summary>
		/// Kick したユーザーの nick を取得します。
		/// </summary>
		public string WhomNick { get; private set; }
		/// <summary>
		/// Kick したユーザーのユーザー名を取得します。
		/// </summary>
		public string WhomIdent { get; private set; }
		/// <summary>
		/// Kick したユーザーが自分であるか取得します。
		/// </summary>
		public bool WhomIsMe { get; private set; }
		/// <summary>
		/// Kick されたユーザーの nick を取得します。
		/// </summary>
		public string WhoNick { get; private set; }
		/// <summary>
		/// Kick されたユーザーが自分であるか取得します。
		/// </summary>
		public bool WhoIsMe { get; private set; }
		/// <summary>
		/// 取得可能な場合、コマンドのメッセージを取得します。
		/// </summary>
		public string Message { get; private set; }

		public IrcKickEventArgs(IrcMessage message, string channel, string nick, string ident, bool isMe, string userMessage,
								string who, bool whoIsMe)
			: base(message)
		{
			this.Channel = channel;
			this.WhomNick = nick;
			this.WhomIdent = ident;
			this.WhomIsMe = isMe;
			this.Message = userMessage;
			this.WhoNick = who;
			this.WhoIsMe = whoIsMe;
		}
	}
}
