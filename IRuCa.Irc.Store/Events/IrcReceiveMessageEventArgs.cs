﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Events
{
	public class IrcReceiveMessageEventArgs : IrcEventArgs
	{
		/// <summary>
		/// メッセージを送信したユーザーの nick を取得します。
		/// </summary>
		public string Nick { get; private set; }
		/// <summary>
		/// メッセージを送信したユーザーのユーザー名を取得します。
		/// </summary>
		public string Ident { get; private set; }
		/// <summary>
		/// メッセージを送信したユーザーのホスト名を取得します。
		/// </summary>
		public string Host { get; private set; }

		/// <summary>
		/// 受信したメッセージを取得します。
		/// </summary>
		public string Message { get; private set; }
		/// <summary>
		/// メッセージを受信したチャンネルまたは nick を取得します。
		/// </summary>
		public string Target { get; private set; }

		public IrcReceiveMessageEventArgs(IrcMessage message, string nick, string ident, string host)
			: base(message)
		{
			this.Nick = nick;
			this.Ident = ident;
			this.Host = host;
			this.Message = message.Trailing;
			this.Target = message.Middle.FirstOrDefault();
		}
	}
}
