﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Events
{
	public class IrcEventArgs : EventArgs
	{
		public IrcMessage IrcMessage { get; private set; }

		public IrcEventArgs(IrcMessage message)
		{
			this.IrcMessage = message;
		}
	}
}
