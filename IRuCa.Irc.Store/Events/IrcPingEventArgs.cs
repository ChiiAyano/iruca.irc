﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Events
{
	public class IrcPingEventArgs : EventArgs
	{
		public string Target { get; private set; }

		public IrcPingEventArgs(string host)
		{
			this.Target = host;
		}
	}
}
