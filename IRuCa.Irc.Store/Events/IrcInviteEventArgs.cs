﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Events
{
	public class IrcInviteEventArgs : IrcEventArgs
	{
		/// <summary>
		/// チャンネルを取得します。
		/// </summary>
		public string Channel { get; private set; }
        ///// <summary>
        ///// Invite したユーザーの nick を取得します。
        ///// </summary>
        //public string WhomNick { get; private set; }
        ///// <summary>
        ///// Invite したユーザーのユーザー名を取得します。
        ///// </summary>
        //public string WhomIdent { get; private set; }
        /// <summary>
        /// Invite したユーザーが自分であるか取得します。
        /// </summary>
        public bool WhomIsMe { get; private set; }
        ///// <summary>
        ///// Invite されたユーザーの nick を取得します。
        ///// </summary>
        //public string WhoNick { get; private set; }
        /// <summary>
        /// Invite されたユーザーが自分であるか取得します。
        /// </summary>
        public bool WhoIsMe { get; private set; }
		/// <summary>
		/// 取得可能な場合、コマンドのメッセージを取得します。
		/// </summary>
		public string Message { get; private set; }

        /// <summary>
        /// Invite したユーザー情報を取得します。
        /// </summary>
        public IrcUser WhomUser { get; private set; }
        /// <summary>
        /// Invite されたユーザー情報を取得します。
        /// </summary>
        public IrcUser WhoUser { get; private set; }

        //public IrcInviteEventArgs(IrcMessage message, string channel, string nick, string ident, bool isMe, string userMessage,
        //                        string who, bool whoIsMe)
        //    : base(message)
        //{
        //    this.Channel = channel;
        //    this.WhomNick = nick;
        //    this.WhomIdent = ident;
        //    this.WhomIsMe = isMe;
        //    this.Message = userMessage;
        //    this.WhoNick = who;
        //    this.WhoIsMe = whoIsMe;
        //}

	    public IrcInviteEventArgs(IrcMessage message, string channel, IrcUser whomUser, bool whomIsMe, IrcUser whoUser,
	                              bool whoIsMe)
	        : base(message)
	    {
	        this.Channel = channel;
	        this.WhomUser = whomUser;
	        this.WhomIsMe = whomIsMe;
	        this.WhoUser = whoUser;
	        this.WhoIsMe = whoIsMe;
	    }
	}
}
