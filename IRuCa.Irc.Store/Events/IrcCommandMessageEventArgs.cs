﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Events
{
	public class IrcCommandMessageEventArgs : IrcEventArgs
	{
		/// <summary>
		/// チャンネルを取得します。
		/// </summary>
		public string Channel { get; private set; }
		/// <summary>
		/// ユーザーの nick を取得します。
		/// </summary>
		public string Nick { get; private set; }
		/// <summary>
		/// ユーザーのユーザー名を取得します。
		/// </summary>
		public string Ident { get; private set; }
		/// <summary>
		/// ユーザーが自分であるか取得します。
		/// </summary>
		public bool IsMe { get; private set; }
		/// <summary>
		/// 取得可能な場合、コマンドのメッセージを取得します。
		/// </summary>
		public string Message { get; private set; }

		public IrcCommandMessageEventArgs(IrcMessage message, string channel, string nick, string ident, bool isMe, string userMessage)
			:base(message)
		{
			this.Channel = channel;
			this.Nick = nick;
			this.Ident = ident;
			this.IsMe = isMe;
			this.Message = userMessage;
		}
	}
}
