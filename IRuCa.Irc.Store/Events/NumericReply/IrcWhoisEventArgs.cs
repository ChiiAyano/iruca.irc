﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Events.NumericReply
{
	public class IrcWhoisEventArgs : IrcEventArgs
	{
		public WhoisData Whois { get; private set; }

		public IrcWhoisEventArgs(IrcMessage message, WhoisData whois)
			: base(message)
		{
			this.Whois = whois;
		}
	}
}
