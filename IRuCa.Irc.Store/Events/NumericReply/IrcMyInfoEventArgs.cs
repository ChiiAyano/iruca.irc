﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Events.NumericReply
{
	public class IrcMyInfoEventArgs : IrcEventArgs
	{
		public string ServerName { get; private set; }
		public string Version { get; private set; }
		public string AvailableUserModes { get; private set; }
		public string AvailableChannelModes { get; private set; }

		public IrcMyInfoEventArgs(IrcMessage message, string serverName, string version, string availableUserModes, string availableChannelModes)
			:base(message)
		{
			this.ServerName = serverName;
			this.Version = version;
			this.AvailableUserModes = availableUserModes;
			this.AvailableChannelModes = availableChannelModes;
		}
	}
}
