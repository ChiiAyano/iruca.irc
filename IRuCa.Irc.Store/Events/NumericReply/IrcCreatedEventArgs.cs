﻿namespace IRuCa.Irc.Events.NumericReply
{
	public class IrcCreatedEventArgs : IrcEventArgs
	{
		public string Date { get; private set; }

		public IrcCreatedEventArgs(IrcMessage message,string date)
			: base(message)
		{
			this.Date = date;
		}
	}
}
