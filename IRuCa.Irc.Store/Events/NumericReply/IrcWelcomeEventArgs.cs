﻿namespace IRuCa.Irc.Events.NumericReply
{
	public class IrcWelcomeEventArgs : IrcEventArgs
	{
		public string WelcomeMessage { get; private set; }

		public IrcWelcomeEventArgs(IrcMessage message, string welcome)
			: base(message)
		{
			this.WelcomeMessage = welcome;
		}
	}
}
