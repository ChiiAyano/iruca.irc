﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Events.NumericReply
{
    public class IrcInviteCompletedEventArgs:IrcEventArgs
    {
        /// <summary>
        /// Invite を送信したチャンネルを取得します。
        /// </summary>
        public IrcChannel Channel { get; private set; }
        /// <summary>
        /// Invite を送信したユーザーを取得します。
        /// </summary>
        public IrcUser User { get; private set; }

        public IrcInviteCompletedEventArgs(IrcMessage message, IrcChannel channel, IrcUser user)
            : base(message)
        {
            this.Channel = channel;
            this.User = user;
        }
    }
}
