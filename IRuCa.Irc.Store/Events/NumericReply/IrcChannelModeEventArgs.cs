﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Events.NumericReply
{
	public class IrcChannelModeEventArgs : IrcEventArgs
	{
		public string Channel { get; private set; }
		public string Mode { get; private set; }
		public string ModeParams { get; private set; }

		public IrcChannelModeEventArgs(IrcMessage message, string channel, string mode, string modeParams)
			: base(message)
		{
			this.Channel = channel;
			this.Mode = mode;
			this.ModeParams = modeParams;
		}
	}
}
