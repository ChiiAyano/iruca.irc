﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Events.NumericReply
{
	public class IrcWhoWasEventArgs : IrcEventArgs
	{
		public IEnumerable<IrcUser> User { get; private set; }

		public IrcWhoWasEventArgs(IrcMessage message, IEnumerable<IrcUser> user)
			:base(message)
		{
			this.User = user;
		}
	}
}
