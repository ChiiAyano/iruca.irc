﻿namespace IRuCa.Irc.Events.NumericReply
{
	public class IrcYourHostEventArgs : IrcEventArgs
	{
		public string ServerName { get; private set; }

		public string ServerVersion { get; private set; }

		public IrcYourHostEventArgs(IrcMessage message, string serverName, string version)
			: base(message)
		{
			this.ServerName = serverName;
			this.ServerVersion = version;
		}
	}
}
