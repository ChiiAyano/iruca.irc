﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Events.NumericReply
{
	public class IrcAwayEventArgs:IrcEventArgs
	{
		public string Nick { get; private set; }
		public string Message { get; private set; }
		public bool IsMe { get; private set; }

		public IrcAwayEventArgs(IrcMessage message, string nick, string awayMessage,bool isMe)
			:base(message)
		{
			this.Nick = nick;
			this.Message = awayMessage;
			this.IsMe = isMe;
		}
	}
}
