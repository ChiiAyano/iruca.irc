﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Events
{
	public class IrcServerMessageEventArgs : IrcEventArgs
	{
		public IrcServerMessageEventArgs(IrcMessage message)
			: base(message)
		{
		}
	}
}
