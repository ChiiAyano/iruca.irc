﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Events
{
    public class IrcTopicChangedEventArgs : IrcEventArgs
    {
        /// <summary>
        /// トピックが変更されたチャンネルを取得します。
        /// </summary>
        public IrcChannel Channel { get; private set; }
        /// <summary>
        /// 新しいトピックを取得します。
        /// </summary>
        public string NewTopic { get; private set; }
        /// <summary>
        /// 誰がトピックを変更したか取得します。トピックを確認したときには表示されません。
        /// </summary>
        public IrcUser Who { get; private set; }


        public IrcTopicChangedEventArgs(IrcMessage message, IrcChannel channel, string topic, IrcUser user)
            : base(message)
        {
            this.Channel = channel;
            this.NewTopic = topic;
            this.Who = user;
        }
    }
}
