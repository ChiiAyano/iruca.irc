﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc.Events
{
	public class IrcMessageSentEventArgs : IrcEventArgs
	{
		public string RawMessage { get; private set; }

		public IrcMessageSentEventArgs(IrcMessage message)
			:base(message)
		{
			this.RawMessage = message.RawMessage;
		}
	}
}
