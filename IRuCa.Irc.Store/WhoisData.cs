﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc
{
	/// <summary>
	/// WHOIS した結果を格納します。
	/// </summary>
	public class WhoisData
	{
		/// <summary>
		/// Nick を取得します。
		/// </summary>
		public string Nick
		{
			get
			{
				if (this.User != null) return this.User.Nick;
				return string.Empty;
			}
		}

		/// <summary>
		/// User Name を取得します。
		/// </summary>
		public string Ident
		{
			get
			{
				if (this.User != null) return this.User.Ident;
				return string.Empty;
			}
		}
		/// <summary>
		/// Host を取得します。
		/// </summary>
		public string Host
		{
			get
			{
				if (this.User != null) return this.User.Host;
				return string.Empty;
			}
		}
		/// <summary>
		/// Real Name を取得します。
		/// </summary>
		public string RealName
		{
			get
			{
				if (this.User != null) return this.User.RealName;
				return string.Empty;
			}
		}
		/// <summary>
		/// ユーザー情報を取得します。
		/// </summary>
		public IrcUser User { get; internal set; }
		/// <summary>
		/// このユーザーが IRC の管理者であるかを取得します。
		/// </summary>
		public bool IrcOperator { get; internal set; }
		/// <summary>
		/// このユーザーがログインしているサーバー名を取得します。
		/// </summary>
		public string Server { get; internal set; }
		/// <summary>
		/// このユーザーがログインしているサーバーの情報を取得します。
		/// </summary>
		public string ServerInfo { get; internal set; }
		/// <summary>
		/// このユーザーのアイドル時間を取得します。
		/// </summary>
		public int IdleTime { get; internal set; }
		/// <summary>
		/// このユーザーが入室しているチャンネル一覧を取得します。
		/// </summary>
		public IEnumerable<IrcUserJoinedChannel> Channels { get; internal set; }

		public override string ToString()
		{
			return string.Format("{0}!{1}@{2} ({3}) :: Server: {4} ({5}), IrcOperator: {6}, {7} channels are joined", this.Nick, this.Ident,
								this.Host, this.RealName, this.Server, this.ServerInfo, this.IrcOperator, this.Channels.Count());
		}
	}
}
