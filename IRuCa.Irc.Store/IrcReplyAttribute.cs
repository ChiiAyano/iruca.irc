﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc
{
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
	public class IrcReplyAttribute : Attribute
	{
		public string ReplyType { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="replyType"></param>
		public IrcReplyAttribute(string replyType)
		{
			this.ReplyType = replyType;
		}
	}
}
