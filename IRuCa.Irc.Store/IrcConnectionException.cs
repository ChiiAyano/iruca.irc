﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRuCa.Irc
{
	public class IrcConnectionException:Exception
	{
		//public string Status { get; private set; }

		public IrcConnectionException()
			:base("An IRC Error.")
		{

		}

		public IrcConnectionException(string message)
			: base(message)
		{

		}
	}
}
